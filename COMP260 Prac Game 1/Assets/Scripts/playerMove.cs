﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour {

    // PUBLIC VARIABLES
    public Vector2 velocity;
    public float maxSpeed = 5.0f;
    public float acceleration = 1.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;
    public string Horizontal, Vertical;

    private float speed = 0.0f;
    private beeSpawner beeSpawner;
    public float destroyRadius = 1.0f;


    // Use this for initialization
    void Start () {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<beeSpawner>();
    }

	// Update is called once per frame
	void Update () {
        playerMove[] player = FindObjectsOfType<playerMove>();

        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(player[1].transform.position, destroyRadius);
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(player[0].transform.position, destroyRadius);
        }
        // the horizontal axis controls the turn
        float turn = Input.GetAxis(Horizontal);

        // turn the car
        transform.Rotate(0, 0, -turn * (turnSpeed * speed) * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(Vertical);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = Mathf.Max(speed - brake * Time.deltaTime, 0);
            }
            else if (speed < 0)
          {
               speed = Mathf.Min(speed + brake * Time.deltaTime, 0);
          }
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;


        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
