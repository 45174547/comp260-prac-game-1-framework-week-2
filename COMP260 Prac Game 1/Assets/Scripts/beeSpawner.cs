﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beeSpawner : MonoBehaviour {

    public int minBees = 0;
    public int maxBees = 50;
    public beeMove beePrefab;
    public float xMin, yMin;
    public float width, height;
    public Rect spawnRect;
    public float beePeriod;
    public float minBeePeriod = 1;
    public float maxBeePeriod = 5;
    private float count;

	// Use this for initialization
	void Start () {
        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);

	}
	
	// Update is called once per frame
	void Update () {
        if (count >= beePeriod)
        {
            if (minBees < maxBees)
            {
                SpawnBee();
                count = 0;
                beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
            }
        }
        count += Time.deltaTime;
	}

    void SpawnBee()
    {
        // instantiate a bee
        beeMove bee = Instantiate(beePrefab);
        // atttach to this object in the hierarchy
        bee.transform.parent = transform;
        // give the bee a name and number
        bee.gameObject.name = "Bee";
        /* move the bee to a random position within
        * the spawn rectangle
        */
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;

        bee.transform.position = new Vector2(x, y);
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within 'radius' of 'centre'
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if(v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

}
