﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beeMove : MonoBehaviour {

    public float speed = 4.0f;  // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    public Transform target1, target2;
    public Vector2 heading = Vector2.right;
    public Vector2 direction;
    public ParticleSystem explosionPrefab;

    // Use this for initialization
    void Start () {
        // find a player object to be the target by type
        playerMove [] player = FindObjectsOfType<playerMove>();
        target1 = player[0].transform;
        target2 = player[1].transform;
	}

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, 1);
    }

    // Update is called once per frame
    void Update () { 
        // get the vector from the bee to the target
        Vector2 direction1 = target1.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        

        // determines whether closer to target1 or target2
        if (direction1.magnitude < direction2.magnitude)
        {
            direction = direction1;
            Debug.Log("Direction1 " + direction1.magnitude);
        }
        else
        { 
            direction = direction2;
            Debug.Log("Direction2 " + direction2.magnitude);
        }

        // caluclate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);

    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, direction);
    }

}
